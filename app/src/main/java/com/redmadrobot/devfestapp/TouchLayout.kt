package com.redmadrobot.devfestapp

import android.content.Context
import android.util.AttributeSet
import android.view.MotionEvent
import android.view.View
import android.widget.FrameLayout

class TouchLayout(context: Context, attrs: AttributeSet) : FrameLayout(context, attrs) {

    override fun onTouchEvent(event: MotionEvent?): Boolean {
        return (parent as View).onTouchEvent(event)
    }

}